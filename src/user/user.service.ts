import { Injectable, NotFoundException } from "@nestjs/common";
import { user } from "./schemes/user.schemes";



@Injectable()
export class Userservice{
  user:user[]=[];

  insertUser(title:string ,desc:string ,price:number ){
      const userId = Math.random.toString();
      const newUser=new user(userId,title,desc,price);
      this.user.push(newUser);
      return userId;
  }

  getAllUsers(){
    return [...this.user];
  }

  getAllUser(userId: string){
    const user = this.user.find((userId)=> user.id === userId);
    if(!user){
      throw new NotFoundException('Could not find product');
    }else{
    return {...user}
    }
  }
}