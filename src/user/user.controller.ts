import { Controller, Get ,Headers, Header, Post, Body, Param} from '@nestjs/common';
import { getCustomRepository } from 'typeorm';
import { Userservice } from './user.service';

@Controller('user')
export class UserController {

  constructor(private readonly userService: Userservice){}


  @Post()
  addProduct(
    @Body('title') title:string,
    @Body('description') desc:string,
    @Body('price') price:number,
    ): any {
  
     const userId = this.userService.insertUser(title,desc,price)
      return {id: userId};

    }

    @Get()
    getAllUser(){
    return this.userService.getAllUsers();
    }

    @Get('/')
    //@Header('content-Type','aoolication-json')
    findAll(): string {
    return 'This action returns all cats';
  };


  @Get('/getAll')
  //@Header('content-type','application-json')
  getusers(){
  return {"user":true};
  }

  @Get(':id')
  getUser(@Param('id') userId: string){

    return this.userService.getAllUser(userId);
  }



}
