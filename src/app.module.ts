import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './user/user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModel } from './user/user.model';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/nest'),
    UserModel
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
